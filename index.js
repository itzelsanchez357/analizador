function verificar(){
		// Se obtiene el valor de los textArea respecto a su id
		var expresion = document.getElementById('expresion').value;
		var resultado = document.getElementById('resultado');
		var semantico = document.getElementById('semantico');

		// Se guardar la expresión en variable 
		var expRegular = /\s*\s*/;

		var dig = Array(), ls = Array(),igual = Array(),digitoFinal = Array(),expOperacion = Array();
	
		// Se separa la cadena que se escribio en "expresion" por caracter y se guarda en un arreglo
		var j = expresion.split(expRegular);
		var extension = j.length, valorSemantico;
		var  digitoFinal,cierreP = 0, aperturaP = 0,vLs=Array(), cierreL = 0, aperturaL = 0;
		
		// Recorrer j 
		for (var valor in j){
			if (/^[a-zA-Z]*$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular
				ls.push(j[valor]);
				vLs=ls.join("");
				resultado.innerHTML = resultado.innerHTML + j[valor] + "\t es variable\n";

				// Se muestra en "resultado" el elemento escrito y el mensaje "es variable"
			}else if (/^[=]$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular
				igual.push(j[valor]);

				resultado.innerHTML = resultado.innerHTML + j[valor] + "\t es un operador de asignación\n";
				// Se muestra en "resultado" el elemento escrito y el mensaje "es un operador de asignación"
			} else if (/^[1-9]|[0-9]$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular
				dig.push(j[valor]);
				// Añadir los valores al array

				digitoFinal = dig.join("");
				// Juntar/unir los valores para reconocer que se pueda añadir mas de un digito
				
				resultado.innerHTML +=  j[valor] + "\t es un número\n";
				//dig.pop(j[valor]);
				// Se muestra en "resultado" el elemento escrito y el mensaje "es un número"
			} else if (/^[+]$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular

				resultado.innerHTML = resultado.innerHTML + j[valor] + "\t es un operador de suma\n";
				// Se muestra en "resultado" el elemento escrito y el mensaje "es un operador de suma"
			}else if (/^[-]$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular

				resultado.innerHTML = resultado.innerHTML + j[valor] + "\t es un operador de resta\n";
				// Se muestra en "resultado" el elemento escrito y el mensaje "es un operador de resta"
			}else if (/^[/]$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular

				resultado.innerHTML = resultado.innerHTML + j[valor] + "\t es división\n";
				// Se muestra en "resultado" el elemento escrito y el mensaje "es división"
			}else if (/^[*]$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular

				resultado.innerHTML= resultado.innerHTML + j [valor] + "\t es un operador de multiplicación\n";
				// Se muestra en "resultado" el elemento escrito y el mensaje "es un operador de multiplicación"
			}else if (/^[(]$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular

				resultado.innerHTML= resultado.innerHTML + j [valor] + "\t es apertura de parentesis\n";
				// Se muestra en "resultado" el elemento escrito y el mensaje "es abertura de parentesis"
				++aperturaP;
				// contador
			}else if (/^[)]$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular

				resultado.innerHTML= resultado.innerHTML + j [valor] + "\t es cierre de parentesis\n";
				// Se muestra en "resultado" el elemento escrito y el mensaje "es cierre de parentesis"
				++cierreP;
				// contador
			}else if (/^[;]$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular

				resultado.innerHTML= resultado.innerHTML + j [valor] + "\t es carácter punto y coma\n";
				// Se muestra en "resultado" el elemento escrito y el mensaje "es carácter punto y coma"
			}else if (/(var)/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular

				resultado.innerHTML= resultado.innerHTML + j [valor] + "\t palabra reservada\n";
				// Se muestra en "resultado" el elemento escrito y el mensaje "es carácter punto y coma"
			}else if(/^[{]$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular
				resultado.innerHTML+= j [valor] + "\t apertura de llave\n";
				// Se muestra en "resultado" el elemento escrito y el mensaje
				++aperturaL;
				// Contador 
			}else if(/^[}]$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular
				resultado.innerHTML+= j [valor] + "\t cierre de llave\n";
				// Se muestra en "resultado" el elemento escrito y el mensaje
				++cierreL;
				// contador
			}else if(/^[\n]$/.test(j[valor])){
				// Se comprueba que la cadena corresponda con la expresion regular
				resultado.innerHTML+= j [valor] + "\t \n";
				// Se muestra en "resultado" el elemento escrito y el mensaje
			}

		}

		//alert(vLs);
		
		
		//resultado.innerHTML += vLs + "\t es palabra reservada!\n";
		//resultado.innerHTML += digitoFinal + "\t es numero!\n";
		
		var valorSintactico= document.getElementById('sintactico');

		// Comprueba que el numero de elemento en la apertura de parentesis sea diferente al de cierre
		if(aperturaP!=cierreP){
			// Si el número de apertura es menor al de cierre muestra mensaje, de lo contrario, falta cierre 
			if(aperturaP<cierreP){
				valorSintactico.innerHTML+= "Falta apertura de parentesis \n";
				semantico.innerHTML+= "Error \n";
			}else{
				valorSintactico.innerHTML+= "Falta cierre de parentesis \n";
				semantico.innerHTML+= "Error \n";
			}
		}else if(aperturaP==0 || cierreP==0){
			valorSintactico.innerHTML+= "";
		}else{
			valorSintactico.innerHTML+= "Parentesis correctos \n";
		}

		// Comprueba que el numero de elemento en la apertura de llaves sea diferente al de cierre
		if(aperturaL!=cierreL){
			// Si el número de apertura es menor al de cierre muestra mensaje, de lo contrario, falta cierre
			if(aperturaL<cierreL){
				valorSintactico.innerHTML+= "Falta apertura de llaves \n";
				semantico.innerHTML+= "Error \n";
			}else{
				valorSintactico.innerHTML+= "Falta cierre de llaves \n";
				semantico.innerHTML+= "Error \n";
			}
		}else if(aperturaL==0 || cierreL==0){
			valorSintactico.innerHTML+= "";
		}else{
			valorSintactico.innerHTML+= "Llaves correctas \n";
		}

		
		// Comienzo de arreglos con los elementos que tendrá la tupla
		Q = [0,1,2,3,4,5,6,7,8,9];
		Z = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
		Y = ["+","-","*","/"];
		H = ["="];
		P = ["(",")"];
		
		/*
			TUPLA
			G {{E,N,D,I,L},{0...9,a-z,+,-,*,/,(,),=},R,E}
			
			DEFINICION DE REGLAS
			E->E+E
			E->E*E
			E->E-E
			E->E/E
			E->(E)
			E->N
			N->DN
			D->Q
			N->LN
			L->Z
			I->N=N
		*/
		

		// Funcion que comprueba  que se inicie con una constante 
		function i(expresion){
			// Se recorre el arreglo Z donde se encuentran los constante
			for (var sintactico in Z){

				// Si el primer valor de la expresion ingresada es igual a una constante muestra un mensaje
				if(expresion.substring(0,1) == Z[sintactico]){
					valorSintactico.innerHTML+=  "Definicion de variable correcto\n"
				}
				
			}
			
			for (var sintactico in H){
				if(expresion.substring(1,2) == H[sintactico]){
					valorSintactico.innerHTML+= "Asignación correcta\n"
				}
				// Si el segundo valor de la expresion ingresada es igual a un "=" muestra un mensaje
			}

		}
		// Llamar a las funciones
		i(expresion);
		
	}
	function limpiar(){
		// Se obtiene a los elementos respecto a su ID
		var elementoUno = document.getElementById('expresion');
		var elementoDos = document.getElementById('resultado');
		var valorSintactico= document.getElementById('sintactico');
		var semantico = document.getElementById('semantico');

		// A los elementos se le añade que sean vacios
		
    	elementoUno.innerHTML = "";
    	elementoDos.innerHTML = "";
    	sintactico.innerHTML = "";
    	semantico.innerHTML = "";
	}


